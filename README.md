# 名称
lzchrome

# 版本
1.0.0

chrome pac文件生成器。
与firefox pac生成器功能相同
相关说明见https://trello.com/c/ZkC5bX3e/346-firefox

# 介绍
通过开启一个http代理服务器(代理服务器使用了https://pypi.python.org/pypi/proxy.py/0.1)，
proxy.py --hostname 0.0.0.0 --port 8899
并根据代理服务器的IP和端口，生成一个pac文件，并用WEB服务器提供其连接URL。

同时开启网絡数据抓包。

当用户在自已的电脑上设置本pac后，上网数据通索对所抓包的http分析，找到用户的cookie，
可以得到邮件的下载。

# git主页
https://bitbucket.org/imagination2013/lzchrome

# 部署信息
服务器 *

端口 *
13600

路径 /opt/CloudProject/lzchrome


# 启动服务
sh run.sh start

# 停止服务
sh run.sh stop

# 注意
monit配置
cp lzchrome_monit.conf /etc/monit/conf.d/
chmod +x /opt/CloudProject/lzchrome/run.sh
monit reload

# 初始化数据库
mysql -uroot -proot -e "create database lzchrome default character set utf8 collate utf8_general_ci";
python manage.py syncdb --noinput