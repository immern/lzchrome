# -*- coding: utf-8 -*-
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.servers.basehttp import FileWrapper

import base64
import re
import md5
import json
import os
import copy
import shutil
import uuid
import sqlite3
import struct
import zipfile
import tempfile

from models import *

from django.conf import settings


def login(request):
    return createjk(request)


def createjk(request):
    ''' 创建监控 '''
    
    if request.method == "POST":
        proxyurl = request.POST.get('proxy')
        if proxyurl:
            Proxysetting(proxyurl=proxyurl).save()
            return HttpResponseRedirect('/managejk/')
        
    countrys = WebSite.objects.values('country').distinct()
    websites = WebSite.objects.all().order_by("country")
    return render_to_response('selectwebsite.html',locals())


def managejk(request):
    ''' 管理监控 '''
    
    psid=request.GET.get('id','')
    if psid:
        try:
            proxyobj = Proxysetting.objects.get(id=psid)            
            if proxyobj.browserdbfilename and os.path.exists(os.path.join(settings.BASE_DIR, 'browser', 'tmp', proxyobj.browserdbfilename)):
                os.remove(os.path.join(settings.BASE_DIR, 'browser', 'tmp', proxyobj.browserdbfilename))            
            if proxyobj.browserfilename and os.path.exists(os.path.join(settings.BASE_DIR, 'browser', 'tmp', proxyobj.browserfilename)):
                os.remove(os.path.join(settings.BASE_DIR, 'browser', 'tmp', proxyobj.browserfilename))                
            proxyobj.delete()
            return HttpResponse('success')
        except Exception,e:
            print e
            return HttpResponse('fail')
        
        
    data_list = Proxysetting.objects.all().order_by('-id')
    paginator = Paginator(data_list, 10)
    page = request.GET.get("page","1")
    try:
        datas = paginator.page(page)
    except PageNotAnInteger:
        page = 1
        datas = paginator.page(1)
    except EmptyPage:
        page = paginator.num_pages
        datas = paginator.page(paginator.num_pages)
    
    countpage = paginator.num_pages    
    
    return render_to_response('managejk.html',locals())


def modifysqdb(pacstr,dbpath):
    ''' pacstr is startwith data:;base64  dbpath is the slitedb path'''
    
    pacstr=pacstr.decode('utf-8')
    sqdb=sqlite3.connect(dbpath)
    cur=sqdb.cursor()
    cur.execute('select value from ItemTable where key="profiles"')
    tmpstr=str(cur.fetchone()[0])
    tmpstr=tmpstr.decode('utf-16')
    js=json.loads(tmpstr)
    js['GoAgent']['proxyConfigUrl']=pacstr
    js['GoAgent']['proxyHttp']=pacstr
    js['GoAgent']['proxyMode']=u'manual'
    tmpstr=json.dumps(js,ensure_ascii=False,encoding='utf-16',sort_keys=True)
    tmpstr=tmpstr.encode('utf-16')[2:]
    cur.execute('update ItemTable set value=? where key="profiles"',(buffer(tmpstr),))

    cur.execute('select value from ItemTable where key="selectedProfile"')
    tmpstr=str(cur.fetchone()[0])
    tmpstr=tmpstr.decode('utf-16')
    js=json.loads(tmpstr)
    js['proxyConfigUrl']=pacstr
    js['proxyHttp']=pacstr
    js['proxyMode']=u'manual'
    tmpstr=json.dumps(js,ensure_ascii=False,encoding='utf-16',sort_keys=True)
    tmpstr=tmpstr.encode('utf-16')[2:]
    cur.execute('update ItemTable set value=? where key="selectedProfile"',(buffer(tmpstr),))
    sqdb.commit()    


def makechrome(pid, pacstr):
    ''' 创建chrome文件 '''

    tmpstr = '{"firstTime":":]","proxyMode":"auto","proxyServer":"","proxyExceptions":"localhost; 127.0.0.1; <local>","proxyConfigUrl":"data:;base64,","ruleListUrl":"","ruleListReload":"720","ruleListProfileId":"direct","ruleListAutoProxy":false,"switchRules":true,"ruleListEnabled":false,"monitorProxyChanges":true,"preventProxyChanges":false,"quickSwitch":false,"startupProfileId":"","confirmDeletion":true,"refreshTab":false}'
    config = tmpstr.replace('data:;base64,',pacstr)
    config = struct.pack('h'*len(config),*[ord(i) for i in config])

    sqdb = os.path.join(settings.BASE_DIR, 'browser', 'chrome', 'chrome-extension_dpplabbmogkhghncfbfdeeokoefdjegm_0.localstorage')
    uuid1 = uuid.uuid4().hex
    outdb = os.path.join(settings.BASE_DIR, 'browser', 'tmp', uuid1)
    shutil.copyfile(sqdb,outdb)
    modifysqdb(pacstr,outdb)
    chromezip = os.path.join(settings.BASE_DIR, 'browser', 'chrome', 'chrome.zip')
    uuid2 = uuid.uuid4().hex
    outchromezip = os.path.join(settings.BASE_DIR, 'browser', 'tmp', uuid2)
    shutil.copyfile(chromezip, outchromezip)
    zipfile.ZipFile(outchromezip,'a').write(outdb,'chrome/Browser/Data/Default/Local Storage/chrome-extension_dpplabbmogkhghncfbfdeeokoefdjegm_0.localstorage')
    Proxysetting.objects.filter(id=pid).update(browserdbfilename=uuid1, browserfilename=uuid2)
    return outchromezip


def downbrowser(request):
    ''' 下载chrome浏览器 '''
    
    pid=request.GET.get('pacid','')
    if pid:
        try:        
            proxyobj = Proxysetting.objects.get(id=pid)
            if proxyobj.browserfilename:
               browserfile = os.path.join(settings.BASE_DIR, 'browser', 'tmp', proxyobj.browserfilename)
            else:
                pacstr = proxyobj.proxyurl
                browserfile=makechrome(pid, pacstr)
            wrapper=FileWrapper(open(browserfile,'rb'))
            response=HttpResponse(wrapper,mimetype='application/zip')
            response['Content-Length'] = os.path.getsize(browserfile)
            response['Content-Disposition']='attachment;filename=chrome翻墙浏览器.zip'
            return response
        except Exception,e:
            print e
    return HttpResponse('ok')


def managezd(request):
    ''' 添加监控站点 '''
    
    if request.method == 'POST':
        id = request.POST.get("dataid", "")
        datatext = request.POST.get("datatext", "")

        if datatext:
            data_list_n = []
            data_dict = {"webname":"", "domain":"", "selected":True, "country":"", "favicon":""}
            data_list = filter(lambda x: len(x)>5, re.split('[\s]+', datatext))
            copy_data_dict = copy.copy(data_dict)
            for i in data_list:
                if i.startswith("#"):
                    if not(filter(lambda x: x == "", copy_data_dict.values())):
                        data_list_n.append(copy_data_dict)
                    copy_data_dict = copy.copy(data_dict)
                    copy_data_dict["webname"] = i[1:]
                elif i.startswith("domain:"):
                    copy_data_dict["domain"] = i[7:].lower()
                elif i.startswith("select:"):
                    if not i[7:].lower().startswith("y"):
                        copy_data_dict["selected"] = False
                elif i.startswith("country:"):
                    copy_data_dict["country"] = i[8:].lower()
                elif i.startswith("favicon:"):
                    copy_data_dict["favicon"] = i[8:].lower()
                

            if not(filter(lambda x: x == "", copy_data_dict.values())):
                data_list_n.append(copy_data_dict)
            
            for i in data_list_n:
                if id:
                    WebSite.objects.filter(id=id).update(webname=i["webname"], domain=i["domain"], selected=i["selected"], country=i["country"] , favicon=i["favicon"])
                else:
                    if WebSite.objects.filter(domain = i["domain"]).exists():
                        WebSite.objects.filter(domain = i["domain"]).update(domain=i["domain"], selected=i["selected"], country=i["country"] , favicon=i["favicon"])
                    else:                
                        WebSite.objects.create(webname=i["webname"], domain=i["domain"], selected=i["selected"], country=i["country"] , favicon=i["favicon"])
            return HttpResponseRedirect('/managezd/')
            
    else:
        id=request.GET.get('id')
        if id:
            try:
                WebSite.objects.filter(id=id).delete()
                return HttpResponse("success")
            except:
                pass
            return HttpResponse("fail")
    
    data_list = WebSite.objects.all()
    paginator = Paginator(data_list, 10)
    page = request.GET.get("page","1")
    try:
        datas = paginator.page(page)
    except PageNotAnInteger:
        page = 1
        datas = paginator.page(1)
    except EmptyPage:
        page = paginator.num_pages
        datas = paginator.page(paginator.num_pages)
    
    countpage = paginator.num_pages
    return render_to_response('websitedataindex.html', {"datas": datas, "page":page, "countpage":countpage})
 
    
def zdTxt(request):
       
    id = request.GET.get("id","")
    
    if id:
        datapackage_list = WebSite.objects.filter(id=id)
    else:
        datapackage_list = WebSite.objects.all()
    datapackage_txt_list = []
    
    for i in datapackage_list:
        one_datapackage = []        
        one_datapackage.append("#"+i.webname)
        one_datapackage.append("domain:"+i.domain)
        if i.selected:
            one_datapackage.append("select:yes")
        else:
            one_datapackage.append("select:no")
        one_datapackage.append("country:"+i.country)
        one_datapackage.append("favicon:"+i.favicon)       
        datapackage_txt_list.append("\r\n".join(one_datapackage))

    datapackage_txt = "\r\n\r\n".join(datapackage_txt_list)

    if id:
        return HttpResponse(datapackage_txt)
    
    response = HttpResponse(mimetype='text/plain')
    response['Content-Disposition'] = 'attachment;filename=Chrome侦控站点.txt'
    response.write(datapackage_txt)
    return response


def ajaxwebSites(request):
    ''' 站点 国家 '''
    
    countryinfo = request.GET.get("countryinfo", "").strip()
    if countryinfo:
        if countryinfo == "all":
            websites = WebSite.objects.order_by("country")
        else:
            websites = WebSite.objects.filter(country=countryinfo)
        return render_to_response("websitelist.tpl",{"websites": websites})
