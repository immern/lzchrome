<ul class='ui-list-icons fn-mt-20 fn-clear'>
{% for website in websites%}
	<li class='cashier-bank-kj'>
	{% if website.selected %}
		<input type="checkbox" name="site_{{ website.id }}" checked/>
	{% else %}
		<input type="checkbox" name="site_{{ website.id }}"/>
	{% endif %}
		<label class="icon-box limited-coupon" name="site_{{ website.id }}" style=" background:url({{ website.favicon }}) no-repeat center left; background-size:32px;" title="{{website.domain}}" onclick="radios(this)">{{ website.webname}}</label>
	</li>
{% endfor %}
</ul>