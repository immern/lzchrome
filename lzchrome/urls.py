from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
admin.autodiscover()

from views import *
from cloudlogin.views import requires_info


urlpatterns = patterns('',
            # Examples: 
                        
            url(r'^static/(?P<path>.*)$', 'django.views.static.serve',
                                {'document_root': settings.STATIC_PATH}),# 
            
            url(r'^$', login), 
            url(r'^createjk/$', requires_info(createjk)),
#                        
            url(r'^managejk/$', requires_info(managejk)),
            url(r'^downbrowser/$', requires_info(downbrowser)),
#                        
            url(r'^managezd/$', requires_info(managezd)),
            url(r'^zd/txt/$', requires_info(zdTxt)),
#            url(r'^zd/website/$', requires_info(ajaxwebSites)),
            
            url(r'^uad/', include("uad.urls")),
            url(r'^help/', include('cloudhelp.urls')),
)
