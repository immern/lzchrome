from django.db import models

class WebSite(models.Model):
    id = models.AutoField(primary_key=True)
    webname = models.CharField(max_length=255)
    domain = models.CharField(max_length=255)
    selected = models.BooleanField(default=True)
    country = models.CharField(max_length=255)
    favicon = models.CharField(max_length=255)
    ctime = models.DateTimeField(auto_now_add=True)

class Proxysetting(models.Model):
    id = models.AutoField(primary_key=True)
    proxyname = models.CharField(max_length=255)
    proxyurl = models.CharField(max_length=255)
    websiteid = models.CharField(max_length=2048)
    browserdbfilename = models.CharField(max_length=255)
    browserfilename = models.CharField(max_length=255)
    ctime = models.DateTimeField(auto_now_add=True)