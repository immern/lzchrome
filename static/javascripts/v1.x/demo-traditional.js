$(document).ready(function() {

  var clip = new ZeroClipboard($("#d_clip_button"));

  clip.on("load", function (client) {
    debugstr("可以开始复制文本框里的内容");
    client.on("complete", function (client, args) {
      debugstr("文本框内容已经复制到剪贴板");
    });
  });

  clip.on("noFlash", function (client) {
    $(".demo-area").hide();
    debugstr("你的浏览器不支持Flash");
  });

  clip.on("wrongFlash", function (client, args) {
    $(".demo-area").hide();
    debugstr("Flash 10.0.0+ is required but you are running Flash " + args.flashVersion.replace(/,/g, "."));
  });


  // jquery stuff (optional)
  function debugstr(text) {
    $("#d_debug").text(text);
  }

  $("#clear-test").on("click", function () {
    $("#fe_text").val("");
    $("#testarea").val("");
  });

});