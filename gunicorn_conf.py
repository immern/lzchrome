import os

bind = "0.0.0.0:13600"
workers = 2
worker_class = 'tornado'
timeout = 300

name = 'lzchrome'
raw_env = ['DJANGO_SETTINGS_MODULE=lzchrome.settings']

chdir = os.path.abspath(os.path.dirname(__file__))
logdir = os.path.join(chdir, 'logs')
pidfile = os.path.join(logdir, 'gunicorn_server.pid')

user = 'root'
group = 'root'

daemon = True
