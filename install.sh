#!/bin/bash

echo "create db"
mysql -uroot -proot -e "drop database if exists lzchrome"
mysql -uroot -proot -e "create database lzchrome default character set utf8 collate utf8_general_ci"


echo "sync db"
python2.6 manage.py syncdb --noinput -v 0

echo "mv monit conf"
mv *_monit.conf /etc/monit/conf.d/
chmod +x run.sh

echo "install python pkg"
pip2.6 install -r requirements.txt -q

# clear some test file here
